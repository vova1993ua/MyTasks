// HelloUser.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>

using namespace std;

int main()
{
	string str;

	cout << "Please, enter your name: " << endl;


	cin >> str;

	if (str.empty())
		cout << "Error, wrong data!" << endl;
	else
		cout << "Hello, " << str << "!" << endl;

    return 0;
}

