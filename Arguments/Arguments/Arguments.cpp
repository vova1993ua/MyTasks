// Arguments.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

using namespace std;


int main(int argc, char* argv[])
{
	if (argc > 1)
	{
		cout << "Hello " << argv[1] << endl;
	}
	else
	{
		cout << "Not arguments" << endl;
	}

	system("pause");

	return 0;
}

