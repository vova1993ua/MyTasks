#include "stdafx.h"

#include <iostream>
#include <ctime>
#include "FindNumb.h"
using namespace std;


FindNumb::FindNumb()
{
	srand(time(NULL));
	RandNum = rand() % 100 + 1;
}

FindNumb::~FindNumb()
{
}

bool FindNumb::FindNumber(int num)
{
	if (num < RandNum)
	{
		cout << "entered number is less than the desired number" << endl;
		return false;
	}
	else if (num > RandNum)
	{
		cout << "entered number is more than the desired number" << endl;
		return false;
	}
	else
	{
		cout << "The number entered is correct" << endl;
		return true;
	}

}