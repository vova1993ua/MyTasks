// Number.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "FindNumb.h"
#include <string>
#include <iostream>


using namespace std;

int main(int argc, char** argv) {
	FindNumb ob;
	int counter = 0;
	int imp;
	string str;
	bool TrueOrFalse;
	do
	{
		counter++;
		m:
		cout << "Enter the number: " << endl;
		cin >> str;

		imp = atoi(str.c_str());

		if (imp == 0 && str != "0")
		{
			cout << "This is not a number" << endl;
			goto m;
		}

		TrueOrFalse = ob.FindNumber(imp);
	} while (TrueOrFalse == false);
	cout << "Attempts for finding the number is " << counter << endl;

	return 0;
}

